# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Filipe Laíns (FFY00) <lains@archlinux.org>

pkgname=pulseaudio-equalizer-ladspa-ffy00
pkgver=3.0.2
pkgrel=4.3
pkgdesc="A 15-band equalizer for PulseAudio"
arch=('any')
url='https://github.com/pulseaudio-equalizer-ladspa/equalizer'
license=('GPL3')
depends=('python-gobject' 'gtk3' 'swh-plugins' 'pulseaudio' 'bc')
makedepends=('meson')
optdepends=('python2-gobject: python2 support')
provides=('pulseaudio-equalizer-ladspa')
conflicts=('pulseaudio-equalizer-ladspa' 'pulseaudio-equalizer-ladspa-git')
source=("$pkgname-$pkgver.tar.gz::${url}/archive/v${pkgver}.tar.gz"
        "https://metainfo.manjariando.com.br/${pkgname}/com.github.pulseaudio-equalizer-ladspa.Equalizer.metainfo.xml"
        https://metainfo.manjariando.com.br/${pkgname}/pulseaudio-equalizer-ladspa-{48,64,128}.png)
sha256sums=('2dd14d7bdbc806bfa239bae49dbeef8ccd8bbdb53a413bd83d0ca32390ceae6f'
            '4373008a8bd61739130e7db9ce46e583c39e53bf669263e3082e6e2704e93b15'
            '83189a56fed86085fad27a46fef8a1f0517b468ada67f074b3a4c41527fc3c8f'
            'a07b3938a8f5bccc3ca94567e43dc833012e18b19d5111cc4f418c14c23a6c29'
            'ca31777af21cae79972aff40c4d62dc6c4a5773f33827d44fb2340e662f01f3f')

build() {
    mkdir -p "${srcdir}"/equalizer-${pkgver}/build
    cd "${srcdir}"/equalizer-${pkgver}/build

    arch-meson ..

    ninja
 }
 
package() {
    cd "${srcdir}"/equalizer-${pkgver}/build

    DESTDIR="$pkgdir" meson install

    python -m compileall -d /usr/lib "$pkgdir/usr/lib"
    python -O -m compileall -d /usr/lib "$pkgdir/usr/lib"

    # It's GLP3 but has a specific copyright string
    install -Dm 644 ../LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE

    # Appstream
    install -Dm644 "${srcdir}/com.github.pulseaudio-equalizer-ladspa.Equalizer.metainfo.xml" \
    "${pkgdir}/usr/share/metainfo/com.github.pulseaudio-equalizer-ladspa.Equalizer.metainfo.xml"
    mv "${pkgdir}/usr/share/applications/com.github.pulseaudio-equalizer-ladspa.Equalizer.desktop" \
    "${pkgdir}/usr/share/applications/com.github.pulseaudio_equalizer_ladspa.Equalizer.desktop"

    local icon_size icon_dir
        for icon_size in 48 64 128; do
            icon_dir="$pkgdir/usr/share/icons/hicolor/${icon_size}x${icon_size}/apps"

            install -d "$icon_dir"
            install -m644 "${srcdir}/pulseaudio-equalizer-ladspa-${icon_size}.png" "$icon_dir/multimedia-volume-control.png"
        done
}
